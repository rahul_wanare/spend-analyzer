package com.gloresoft.spendanalyzer.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "tag", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
public class Tag {
    @Id
    @NotNull
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "tag")
    private Set<BillingData> billingDataSet = new HashSet<>(); 
}
