import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { SpendSummary } from '../_model/spend_summary';
import { SummaryService } from '../_services/summary.service';


@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  spendSummary: SpendSummary[] = [];
  displayedColumns: string[] = ['tag', 'totalAmount', 'percentage'];
  fromDate = new Date();
  toDate = new Date();

  constructor(private summaryService: SummaryService) { }

  ngOnInit(): void {
  }

  generateSummary(): void {
    
    this.summaryService.generateSummary(this.fromDate, this.toDate)
      .subscribe((dataArr: SpendSummary[]) => {
          this.spendSummary = dataArr;
        },
        error => {
          console.log('Error occurred while fetching spend summary');
        }
      );

  }
 
}
