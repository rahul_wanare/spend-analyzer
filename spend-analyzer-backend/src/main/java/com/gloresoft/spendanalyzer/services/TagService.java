package com.gloresoft.spendanalyzer.services;

import com.gloresoft.spendanalyzer.entities.Tag;

import java.util.List;

public interface TagService {

    void create(String tagName);

    void delete(String tagName);

    List<Tag> getAll();
}
